# Planning

## Research

- [ ] Vocab
- [ ] Functions
- [ ] Methods

## Problem decomposition

- [ ] Input
- [ ] Output
- [ ] Examples
- [ ] Conditions (if)
- [ ] Iteration (loop)

## Problems

### 01 minimum_value

The function takes two values. Our function will compare both values and return the smaller of both values.
if they are equal then it can return either.

### 04 max_of_three

The function takes three arguments and expects that the largest value is returned.

### 06 can_skydive

The function takes two arguments, an age (int) and a has_consent_form (boolean)

the conditions for true are that age is >= 18 and has_consent_form == true
otherwise return false

### 09 is_palindrome

The function takes in a string argument and is supposed to compare if it is the same word backwards and forwards.

1. Take the word and convert it into a list
2. Use the reverse function in list
3. Convert it back into a string
4. Compare the original word to the converted one and return results

### 10 is_divisible_by_3

The function takes one int. Check if the value is divisible by 3, if so then return 'fizz', if not then just return the number.

### 11 is_divisible_by_5

The function takes one int. Check if the value is divisible by 5, if so then return 'buzz', if not then just return the number.

### 12 fizzbuzz

Same as it ever was

### 14 can_make_pasta

The function takes a list and checks if it contains "flour", "eggs" and "oil" and returns true if it does. Otherwise it returns false.

### 16 is_inside_bounds

The function takes two ints and tests both values to see if they're between 0 and 10.

### 19 is_inside_bounds

The function receives two int coordinates which have to be checked to see if they're within a rectangle.

The function also receives x and y positions for the edges and widths of the rectangle

The conditions to determine if our coordinates are as follows:

1. x is greater than or equal to rect_x
2. y is greater than or equal to rect_y
3. x is less than or equal to rect_x + rect_width
4. y is less than or equal to rect_y + rect_height

### 20 has_quorum

The function receives two arguments attendees_list, members_list.

1. Compare to see if the attendees_list is greater than or equal to 50% of the members_list if so return true
2. Otherwise return false

### 22 gear_for_day

The function takes two arguments is_workday and is_sunny

1. If day is not sunny, if its a workday then gear must include "umbrella"
2. If it is a workday then gear must include "laptop"
3. If its not a workday then gear must include surfboard
4. Return the gear list at the end

### 24 calculate_averages

Function receives a list of ints.

1. The function should add up all of the ints and divide them by the number of ints in list and return that value
2. If the list value is empty, it should return None

### 25 calculate_sum

Function takes a list of numbers and returns their sum.

If the list is empty then the function should return None

### 26 calculate_grades

Function takes a list of scores and returns a letter grade based upon the average of the scores.

1. function takes a list of ints (between 0 and 100)
2. The function should calculate the average.
3. The function will return a different letter grade
   depending upon the average

### 27 max_in_list

Function takes a list and should return the largest
value in the list.

If the list is empty then return None

### 28 remove_duplicate_letters

Function takes a string and is supposed to remove any duplicate letters and returns the resulting string

If the list is empty then return the empty string

### 30 find_second_largest

Function takes a list of values and is expected to return the second largest from the list.

If the list of values is empty or has only one then it should return None

### 31 sum_of_squares

Function receives a list of numerical values and returns the sum of each item squared

If the list of values is empty then return None

### 32 sum_of_first_n_numbers(limit)

The function receives an input of one number. It
is supposed to add up all of the numbers starting from 0 up to the provided number and return that value.

If the number is -1 then return None

### 33 sum_of_first_n_even_numbers

Function takes one argument which is the count of numbers to be added together. The count will determine the number of even numbers starting from zero which will be added.

-1 will return None

### 34, 35 count_letters_and_digits

The function takes in a string and is supposed to return a count of letters and also a count of digits in the provided string.

1. create two variables letters, digits
2. iterate over the string and test each character
3. return the values from the two variables

### 37 pad_left

Function takes three parameters:

1. number
2. length of the returned string
3. a padding character

Return
a string

Pseudocode

1. Turn the number into a string
2. If the number is shorter than the desire length
   of the returned string then fill it with the
   padding character

### 38 reverse_dictionary

The function takes a dictionary as a parameter and reverse the keys and values and returns it.

### 41 add_csv_lines

The function takes one input a list.
Each item in the list is a comma separated string of numbers
The function will output a list
The returned list will have each item corresponding to the sum of each item in the input

1. Create a new list to store returned values
2. Iterate through the input list
3. Each item in the list should be looped over with each number added to the sum
4. That sum will be appended onto the return list.

### 42 pairwise_add

Zip two lists together, add their values and return the results of each paired item in a new list

### 43 find_indexes

...

### 44 translate

The function takes two inputs:

1. a list of keys
2. a dictionary

The function returns a new list that:

1. contains values of corresponding keys in the dictionary
2. If the key isn't in the dictionary that it should have None as that entry

Pseudocode
Loop over the keys
Check to see if that key is in the dictionary and if so
then add to new list, if not then add None

### 46 make_sentences

The function takes 3 sets of words to create as many 3 word sentences as it can from them.

Function takes 3 inputs of lists:
subjects, verbs and objects

the function returns a list of as many possible strings as it can make

### 47 password

### 48 count_word_frequencies

input: sentence (string)
output: a dictionary whose keys are the words in the sentence and the value is the # of times they appear

1. split word into a list
2. iterate over list
3. take each word in list into dictionary, set 1
4. increment each time that word repeats

### 49 sum_two_numbers

### 50 halve_the_list

### 51 safe_divide

### 52 generate_lottery_numbers

### 53 username_from_email

### 54 check_input

### 55 simple_roman

input: a number from 1 to 10
output: that number formatted in roman numerals

### 56 num_concat

### 57 sum_fraction_sequence

input: a number
output: a sum of the fractions from 1/2

1. iterate the range of number of number
2. in each iteration add number (numerator) with number+1 (denominator)
3. return sum

### 58 group_cities_by_state

input: a list of cities, formatted by cityname, state initials
output: a dictionary where state initials are key and values are list of the cities

1. iterate over the list of cities
2. parse the state initials from each string
3. create a dictionary entry of the state initial
4. append the name of the city into that state's value.

### 59 specific_random

### 60 only_odds

### 61 remove_duplicates

input: a list of values
output: a copy of the list with all duplicates removed, and also kept in the same order

### 62 basic_calculator

input:
left - number on left
op - math operation to perform
right - number on the right

output: the result of the math operation

### 63 shift_letters

### 64 temperature_differences

inputs:
a set of high temperatures
a set of low temperatures

output:
a list of differences between each high and low

### 65 biggest_gap
