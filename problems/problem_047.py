# Complete the check_password function that accepts a
# single parameter, the password to check.
#
# A password is valid if it meets all of these criteria
#   * It must have at least one lowercase letter (a-z)
#   * It must have at least one uppercase letter (A-Z)
#   * It must have at least one digit (0-9)

#   * It must have at least one special character $, !, or @
#   * It must have six or more characters in it
#   * It must have twelve or fewer characters in it
#
# The string object has some methods that you may want to use,
# like ".isalpha", ".isdigit", ".isupper", and ".islower"

def check_password(password):
    valid = True
    # Check length
    if len(password) < 6 or len(password) > 12:
        return False

    # Check special char
    for char in [ '$', '!','@']:
        if not char in password:
            return False

    # Check for a number
    flag = False
    for char in password:
        if char.isdigit():
            flag = True
    if not flag:
        return False

    # check for letter
    flag = False
    for char in password:
        if char.isalpha():
            flag = True
    if not flag:
        return False

    # check for uppercase
    flag = False
    for char in password:
        if char.isupper():
            flag = True
    if not flag:
        return False

    # check for lowercase
    flag = False
    for char in password:
        if char.islower():
            flag = True
    if not flag:
        return False

    return valid

password = 'A123z456$!@'

result = check_password(password)

print(result)
