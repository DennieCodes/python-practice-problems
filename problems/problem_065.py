# Write a function that meets these requirements.
#
# Name:       biggest_gap
# Parameters: a list of numbers that has at least
#             two numbers in it
# Returns:    the largest gap between any two
#             consecutive numbers in the list
#             (this will always be a positive number)
#
# Examples:
#     * input:  [1, 3, 5, 7]
#       result: 2 because they all have the same gap
#     * input:  [1, 11, 9, 20, 0]
#       result: 20 because from 20 to 0 is the biggest gap
#     * input:  [1, 3, 100, 103, 106]
#       result: 97 because from 3 to 100 is the biggest gap
#
# You may want to look at the built-in "abs" function

def biggest_gap(numbers):
    gaps = []

    for num in range(len(numbers)):
        if num + 1 < len(numbers):
            gaps.append(abs(numbers[num] - numbers[num+1]))

    return max(gaps)

data1 = [1, 3, 5, 7]
data2 = [1, 11, 9, 20, 0]
data3 = [1, 3, 100, 103, 106]
result = biggest_gap(data1)
# print(result)

result = biggest_gap(data2)
# print(result)

result = biggest_gap(data3)
print(result)
