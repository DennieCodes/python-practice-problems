import math
# Write a function that meets these requirements.
#
# Name:       halve_the_list
# Parameters: a single list
# Returns:    two lists, each containing half of the original list
#             if the original list has an odd number of items, then
#             the extra item is in the first list
#
# Examples:
#    * input: [1, 2, 3, 4]
#      result: [1, 2], [3, 4]
#    * input: [1, 2, 3]
#      result: [1, 2], [3]

def halve_the_list(list):
    new_list = []
    half = math.ceil(len(list) / 2)
    new_list.append(list[:half])
    new_list.append(list[half:])

    return new_list

# data = [1, 2, 3, 4]
data = [1, 2, 3]
print(halve_the_list(data))