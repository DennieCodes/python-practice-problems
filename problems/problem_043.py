# Complete the find_indexes function which accepts two
# parameters, a list and a search term. It returns a new
# list that contains the indexes of the search term in
# the search list.
#
# Remember that indexes in Python are zero-based. That
# means the first element in the list is index 0.
#
# Examples:
#   * search_list:  [1, 2, 3, 4, 5]
#     search_term:  4
#     result:       [3]
#   * search_list:  [1, 2, 3, 4, 5]
#     search_term:  6
#     result:       []
#   * search_list:  [1, 2, 1, 2, 1]
#     search_term:  1
#     result:       [0, 2, 4]
#
# Look up the enumerate function to help you with this problem.

# Regular answer
# def find_indexes(search_list, search_term):
#     converted = list(enumerate(search_list))

#     filtered = list(filter(lambda item: item[1] == search_term, converted))

#     result = list(map(lambda item: item[0], filtered))

#     return result

# Lean refactor
def find_indexes(search_list, search_term):
    return list(map(lambda item: item[0], list(filter(lambda item: item[1] == search_term, list(enumerate(search_list))))))

search_list =  [1, 2, 3, 4, 5]
search_term =  4

result = find_indexes(search_list, search_term)
print(result)

search_list = [1, 2, 1, 2, 1]
search_term = 1

result = find_indexes(search_list, search_term)
print(result)