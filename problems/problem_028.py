# Complete the remove_duplicate_letters that takes a string
# parameter "s" and returns a string with all of the
# duplicates removed.
#
# Examples:
#   * For "abc", the result is "abc"
#   * For "abcabc", the result is "abc"
#   * For "abccba", the result is "abc"
#   * For "abccbad", the result is "abcd"
#
# If the list is empty, then return the empty string.

def remove_duplicate_letters(s):
    letter_set = {}
    if len(s) == 0:
        return s

    for letter in s:
        letter_set[letter] = 1

    return ''.join(letter_set)

result = remove_duplicate_letters('abcdefddd')
print(result)
