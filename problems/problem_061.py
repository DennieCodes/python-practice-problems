# Write a function that meets these requirements.
#
# Name:       remove_duplicates
# Parameters: a list of values
# Returns:    a copy of the list removing all
#             duplicate values and keeping the
#             original order
#
# Examples:
#     * input:   [1, 1, 1, 1]
#       returns: [1]
#     * input:   [1, 2, 2, 1]
#       returns: [1, 2]
#     * input:   [1, 3, 3, 20, 3, 2, 2]
#       returns: [1, 3, 20, 2]

# regular solution
def remove_duplicates(values):
    result = []
    for value in values:
        if result.count(value) < 1:
            result.append(value)

    return result

data1 = [1, 1, 1, 1]
data1 = [1, 2, 2, 1]
data3 = [1, 3, 3, 20, 3, 2, 2]

result = remove_duplicates(data3)
print(result)
