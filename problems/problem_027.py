# Complete the max_in_list function to find the
# maximum value in a list
#
# If the list is empty, then return None.
#

def max_in_list(values):
    if len(values) == 0:
        return None

    values.sort()
    return values[len(values) - 1]

result = max_in_list([1, 20, 3, 4, 10])
print(result)