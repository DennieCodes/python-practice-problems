# Write a function that meets these requirements.
#
# Name:       temperature_differences
# Parameters: highs: a list of daily high temperatures
#             lows: a list of daily low temperatures
# Returns:    a new list containing the difference
#             between each high and low temperature
#
# The two lists will be the same length
#
# Example:
#     * inputs:  highs: [80, 81, 75, 80]
#                lows:  [72, 78, 70, 70]
#       result:         [ 8,  3,  5, 10]

# regular answer
# def temperature_differences(highs, lows):
#     combo = list(zip(highs, lows))
#     result = list(map(lambda item: item[0] - item[1], combo))

#     return result

# lean mean refactor
def temperature_differences(highs, lows):
    return list(map(lambda item: item[0] - item[1], list(zip(highs, lows))))

highs = [80, 81, 75, 80]
lows = [72, 78, 70, 70]

print(temperature_differences(highs, lows))
