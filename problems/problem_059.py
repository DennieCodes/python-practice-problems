import random # Write a function that meets these requirements.
#
# Name:       specific_random
# Parameters: none
# Returns:    a random number between 10 and 500, inclusive,
#             that is divisible by 5 and 7
#
# Examples:
#     * returns: 35
#     * returns: 105
#     * returns: 70
#
# Guidance:
#   * Generate all the numbers that are divisible by 5
#     and 7 into a list
#   * Use random.choice to select one

def specific_random():
    random_list = list(filter(lambda item: item % 5 == 0 and item % 7 == 0, range(10, 500)))

    return random_list[random.randint(0, len(random_list)-1)]

result = specific_random()
print(result)