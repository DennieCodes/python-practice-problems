import math

# Write a function that meets these requirements.
#
# Name:       safe_divide
# Parameters: two values, a numerator and a denominator
# Returns:    if the denominator is zero, then returns math.inf.
#             otherwise, returns numerator / denominator
#
# Don't for get to import math!

# regular
# def safe_divide(numerator, denominator):
#     if denominator == 0:
#         return math.inf
#     else:
#         return numerator / denominator

# lean mean refactor
def safe_divide(numerator, denominator):
    return math.inf if denominator == 0 else numerator / denominator

result = safe_divide(2, 10)
print(result)
