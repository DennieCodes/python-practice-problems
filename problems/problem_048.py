# Write a function that meets these requirements.
#
# Name:       count_word_frequencies
# Parameters: sentence, a string
# Returns:    a dictionary whose keys are the words in the
#             sentence and their values are the number of
#             times that word has appeared in the sentence
#
# The sentence will contain now punctuation.
#
# This is "case sensitive". That means the word "Table" and "table"
# are considered different words.
#
# Examples:
#    * sentence: "I came I saw I learned"
#      result:   {"I": 3, "came": 1, "saw": 1, "learned": 1}
#    * sentence: "Hello Hello Hello"
#      result:   {"Hello": 3}

## FUNCTION PSEUDOCODE
# function count_word_frequencies(sentence):
    # words = split the sentence
    # counts = new empty dictionary
    # for each word in words
        # if the word is not in counts
            # counts[word] = 0
        # add one to counts[word]
    # return counts

def count_word_frequencies(sentence):
    result = {}
    split = sentence.split()

    for word in split:
        result[word] = result.setdefault(word, 0) + 1

    return result

sentence1 = "I came I saw I learned"
result1 = count_word_frequencies(sentence1)
print(result1)

# sentence2 = "Hello Hello Hello"
# result2 = count_word_frequencies(sentence2)
# print(result2)